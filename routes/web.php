<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homee');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('halo', function () {
	return "Halo, Selamat datang di Kominfo Kabupaten Mojokerto";
});

// route hardware
Route::get('kategori', 'CategoryController@index');
Route::get('/kategori/hardware/tambah','CategoryController@tambah');
Route::post('hardware/store','CategoryController@store');
Route::get('hardware/edit/{id}','CategoryController@edit');
Route::post('/hardware/update','CategoryController@update');
Route::get('/hardware/hapus/{id}','CategoryController@hapus');

Route::get('inventori', 'Inventorycontroller@inventori');

//route software
Route::get('software', 'softcontroller@softindex');
Route::get('software/software/tambah','softcontroller@softtambah');
Route::post('software/store','softcontroller@store');
Route::get('software/edit/{id}','softcontroller@softedit');
Route::post('software/update','softcontroller@update');
Route::get('software/hapus/{id}','softcontroller@hapus');

//route prokum
Route::get('prokum', 'prokumcontroller@prokumindex');
Route::get('prokum/prokum/tambah','prokumcontroller@protambah');
Route::post('prokum/store','prokumcontroller@store');
Route::get('prokum/edit/{id}','prokumcontroller@proedit');
Route::post('prokum/update','prokumcontroller@update');
Route::get('prokum/hapus/{id}','prokumcontroller@hapus');

//route Ip
Route::get('ip', 'ipcontroller@ipindex');
Route::get('ip/ip/tambah','ipcontroller@iptambah');
Route::post('ip/store','ipcontroller@store');
Route::get('ip/edit/{id}','ipcontroller@ipedit');
Route::post('ip/update','ipcontroller@update');
Route::get('ip/hapus/{id}','ipcontroller@hapus');
