<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class softcontroller extends Controller

{
    public function softindex()
    {
    	// mengambil data dari table software
    	$software = DB::table('software')->get();
 
    	// mengirim data software ke view index
    	return view('softindex',['software' => $software]);
 
    }
		// method untuk menampilkan view form tambah software
		public function softtambah()
	{

		// memanggil view tambah
		return view('softtambah');

	}

	// method untuk insert data ke table software
	public function store(Request $request)
	{
		// insert data ke table software
		DB::table('software')->insert([
			'software_nama' => $request->nama,
			'software_stok' => $request->stok,
			'software_keterangan' => $request->keterangan,
		]);
		// alihkan halaman ke halaman software
		return redirect('/software');

	}

	// method untuk edit data software
	public function softedit($id)
	{
		// mengambil data software berdasarkan id yang dipilih
		$software = DB::table('software')->where('software_id',$id)->get();
		// passing data software yang didapat ke view edit.blade.php
		return view('softedit',['software' => $software]);

	}

	// update data software
	public function update(Request $request)
	{
		// update data software
		DB::table('software')->where('software_id',$request->id)->update([
			'software_nama' => $request->nama,
			'software_stok' => $request->stok,
			'software_keterangan' => $request->keterangan,
		]);
		// alihkan halaman ke halaman software
		return redirect('/software');
	}

	// method untuk hapus data software
	public function hapus($id)
	{
		// menghapus data software berdasarkan id yang dipilih
		DB::table('software')->where('software_id',$id)->delete();
			
		// alihkan halaman ke halaman software
		return redirect('/software');
	}

}


