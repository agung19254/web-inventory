<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller

{
    public function index()
    {
    	// mengambil data dari table hardware
    	$hardware = DB::table('Hardware')->get();
 
    	// mengirim data hardware ke view index
    	return view('index',['Hardware' => $hardware]);
 
    }
		// method untuk menampilkan view form tambah hardware
		public function tambah()
	{

		// memanggil view tambah
		return view('tambah');

	}

	// method untuk insert data ke table hardware
	public function store(Request $request)
	{
		// insert data ke table hardware
		DB::table('hardware')->insert([
			'hardware_nama' => $request->nama,
			'hardware_stok' => $request->stok,
			'hardware_keterangan' => $request->keterangan,
		]);
		// alihkan halaman ke halaman hardware
		return redirect('/kategori');

	}

	// method untuk edit data hardware
	public function edit($id)
	{
		// mengambil data hardware berdasarkan id yang dipilih
		$hardware = DB::table('hardware')->where('hardware_id',$id)->get();
		// passing data hardware yang didapat ke view edit.blade.php
		return view('edit',['hardware' => $hardware]);

	}

	// update data hardware
	public function update(Request $request)
	{
		// update data hardware
		DB::table('hardware')->where('hardware_id',$request->id)->update([
			'hardware_nama' => $request->nama,
			'hardware_stok' => $request->stok,
			'hardware_keterangan' => $request->keterangan,
		]);
		// alihkan halaman ke halaman hardware
		return redirect('/kategori');
	}

	// method untuk hapus data hardware
	public function hapus($id)
	{
		// menghapus data hardware berdasarkan id yang dipilih
		DB::table('hardware')->where('hardware_id',$id)->delete();
			
		// alihkan halaman ke halaman hardware
		return redirect('/kategori');
	}

}


