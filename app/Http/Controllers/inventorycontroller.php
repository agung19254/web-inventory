<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class inventorycontroller extends Controller

{
    public function inventori()
    {
    	// mengambil data dari table inventori
    	$hardware = DB::table('Hardware')->paginate(10);
        $software = DB::table('software')->paginate(10);
        $produk_hukum = DB::table('produk_hukum')->paginate(10);
        $ip_local = DB::table('ip_local')->paginate(10);
 
    	// mengirim data inventori ke view index
    	return view('inventori',['Hardware' => $hardware,'software' => $software,'produk_hukum' => $produk_hukum,'ip_local' => $ip_local]);
 
    }

}


