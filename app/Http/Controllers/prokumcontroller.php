<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class prokumcontroller extends Controller

{
    public function prokumindex()
    {
    	// mengambil data dari table produk hukum
    	$produk_hukum = DB::table('produk_hukum')->get();
 
    	// mengirim data produk hukum ke view index
    	return view('proindex',['produk_hukum' => $produk_hukum]);
 
    }
		// method untuk menampilkan view form tambah produk hukum
		public function protambah()
	{

		// memanggil view tambah
		return view('protambah');

	}

	// method untuk insert data ke table produk hukum
	public function store(Request $request)
	{
		// insert data ke table produk hukum
		DB::table('produk_hukum')->insert([
			'hukum_nama' => $request->nama,
		]);
		// alihkan halaman ke halaman produk hukum
		return redirect('/prokum');

	}

	// method untuk edit data produk hukum
	public function proedit($id)
	{
		// mengambil data produk hukum berdasarkan id yang dipilih
		$produk_hukum = DB::table('produk_hukum')->where('hukum_id',$id)->get();
		// passing data produk hukum yang didapat ke view edit.blade.php
		return view('proedit',['produk_hukum' => $produk_hukum]);

	}

	// update data produk hukum
	public function update(Request $request)
	{
		// update data produk hukum
		DB::table('produk_hukum')->where('hukum_id',$request->id)->update([
			'hukum_nama' => $request->nama,
			]);
		// alihkan halaman ke halaman produk hukum
		return redirect('/prokum');
	}

	// method untuk hapus data produk hukum
	public function hapus($id)
	{
		// menghapus data produk hukum berdasarkan id yang dipilih
		DB::table('produk_hukum')->where('hukum_id',$id)->delete();
			
		// alihkan halaman ke halaman produk hukum
		return redirect('/prokum');
	}

}


