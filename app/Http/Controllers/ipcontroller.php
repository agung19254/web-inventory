<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ipcontroller extends Controller

{
    public function ipindex()
    {
    	// mengambil data dari table ip
    	$ip_local = DB::table('ip_local')->get();
 
    	// mengirim data ip ke view index
    	return view('ipindex',['ip_local' => $ip_local]);
 
    }
		// method untuk menampilkan view form tambah ip
		public function iptambah()
	{

		// memanggil view tambah
		return view('iptambah');

	}

	// method untuk insert data ke table ip
	public function store(Request $request)
	{
		// insert data ke table ip
		DB::table('ip_local')->insert([
			'ip_nama' => $request->nama,
			'ip_nomor' => $request->nomor,
		]);
		// alihkan halaman ke halaman ip
		return redirect('/ip');

	}

	// method untuk edit data ip
	public function ipedit($id)
	{
		// mengambil data ip berdasarkan id yang dipilih
		$ip_local = DB::table('ip_local')->where('ip_id',$id)->get();
		// passing data ip yang didapat ke view edit.blade.php
		return view('ipedit',['ip_local' => $ip_local]);

	}

	// update data ip
	public function update(Request $request)
	{
		// update data ip
		DB::table('ip_local')->where('ip_id',$request->id)->update([
			'ip_nama' => $request->nama,
			'ip_nomor' => $request->nomor,
		]);
		// alihkan halaman ke halaman ip
		return redirect('/ip');
	}

	// method untuk hapus data ip
	public function hapus($id)
	{
		// menghapus data ip berdasarkan id yang dipilih
		DB::table('ip_local')->where('ip_id',$id)->delete();
			
		// alihkan halaman ke halaman ip
		return redirect('/ip');
	}

}
