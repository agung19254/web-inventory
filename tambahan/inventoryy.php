<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 5.1.1
 */

/**
 * Database `inventoryy`
 */

/* `inventoryy`.`hardware` */
$hardware = array(
  array('hardware_id' => '1','hardware_nama' => 'Monitor','hardware_stok' => '8','hardware_keterangan' => 'Baik'),
  array('hardware_id' => '2','hardware_nama' => 'Printer','hardware_stok' => '3','hardware_keterangan' => 'Baik'),
  array('hardware_id' => '7','hardware_nama' => 'kursi','hardware_stok' => '10','hardware_keterangan' => 'Baru'),
  array('hardware_id' => '8','hardware_nama' => 'TV','hardware_stok' => '2','hardware_keterangan' => 'Besar'),
  array('hardware_id' => '14','hardware_nama' => 'cagak','hardware_stok' => '4','hardware_keterangan' => 'kokoh')
);

/* `inventoryy`.`ip_local` */
$ip_local = array(
  array('ip_id' => '3','ip_nama' => 'my pc','ip_nomor' => '172.24.10.1')
);

/* `inventoryy`.`produk_hukum` */
$produk_hukum = array(
  array('hukum_id' => '1','hukum_nama' => 'PERBUP')
);

/* `inventoryy`.`software` */
$software = array(
  array('software_id' => '1','software_nama' => 'microsoft office','software_stok' => '7','software_keterangan' => 'Original'),
  array('software_id' => '4','software_nama' => 'tes','software_stok' => '11','software_keterangan' => 'oy'),
  array('software_id' => '5','software_nama' => 'haha','software_stok' => '99','software_keterangan' => 'ya')
);
