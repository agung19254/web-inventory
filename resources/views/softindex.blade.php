<!DOCTYPE html>
<html>
<head>
	<title>Data Software</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/form.css') }}">
	<script type="text/javascript" src="{{ asset('/js/app.js') }}"></script>
	
	<!-- Favicone Icon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/img/favicon.ico') }}">
    <link rel="icon" type="image/png" href="{{ asset('/img/favicon.png') }}">
    <link rel="apple-touch-icon" href="{{ asset('/img/favicon.png') }}">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/ionicons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/plugin/sidebar-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/plugin/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/jquery-ui.css') }}">
    <!-- SLIDER REVOLUTION CSS SETTINGS -->
    <link href="{{ asset('/plugin/rs-plugin/css/settings.css') }}" rel="stylesheet" type="text/css" media="screen" />

</head>
<body>

<!-- Site Wraper -->
<div class="wrapper">

	<!-- Header -->
	<header id="header" class="header shadow">
		<div class="header-inner">

			<!-- Logo -->
			<div class="logo">
				<a href="/">
					<img class="logo-light" src="img/kominfo.png" alt="KOMINFO" />
				</a>
			</div>
			<!-- End Logo -->

			<!-- Mobile Navbar Icon -->
			<div class="nav-mobile nav-bar-icon">
				<span></span>
			</div>
			<!-- End Mobile Navbar Icon -->

			<!-- Navbar Navigation -->
			<div class="nav-menu">
				<ul class="nav-menu-inner">
					<li>
						<a class="" href="/"> <font color="gold">Home</font></a>
					</li>
					<li>
						<a class="menu-has-sub"> <font color="gold">Category <i class="fa fa-angle-down"> </font></i></a>
						<!-- Dropdown -->
						<ul class="sub-dropdown dropdown">
							<li>
								<a href="/kategori"> Hardware</a>
							</li>
							<li>
								<a href="/software">Software</a>
							</li>
							<li>
								<a href="/prokum">Produk Hukum</a>
							</li>
							<li>
								<a href="/ip">Ip Local</a>
							</li>
						</ul>
						<!-- End Dropdown -->
					</li>
					<li>
						<a class="" href="/inventori"> <font color="gold">Inventori</font></a>
					</li>
					<li>
						<a class="" href="/contact"> <font color="gold">Contact Us</font></a>
					</li>
				</ul>
			</div>
			<!-- End Navbar Navigation -->

		</div>
	</header>
	<!-- End Header -->

<br/><br/><br/><br/><br/>
	<h2 align="center">KOMINFO KABUPATEN MOJOKERTO</h2>
	<h3 align="center">Daftar Data Software</h3> <br/><br/>
	<a href="software/software/tambah" > <center> + Tambah Software Baru </center></a>

	<table border="1" align="center">
		<tr>
			<th>ID</th>
			<th>Nama Software</th>
			<th>Stok</th>
			<th>Keterangan</th>
			<th>Opsi</th>
		</tr>
        
		@foreach($software as $p)
		<tr>
			<td>AS0{{ $p->software_id }}</td>
			<td>{{ $p->software_nama }}</td>
			<td>{{ $p->software_stok }}</td>
			<td>{{ $p->software_keterangan }}</td>
			<td>
				<a href="/software/edit/{{ $p->software_id }}">Edit</a>
				|
				<a href="/software/hapus/{{ $p->software_id }}">Hapus</a>
			</td>
		</tr>
		@endforeach
	</table>

	<!-- JS -->

    <script type="text/javascript" src="{{ asset('/js1/jquery-1.11.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.fitvids.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.viewportchecker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.stellar.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.colorbox-min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/plugin/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/plugin/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.fs.tipper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/mediaelement-and-player.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/jplugin/sidebar-menu.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/revolution-slider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/theme.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/navigation.js') }}"></script>

</body>
</html>