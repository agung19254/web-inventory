﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Selamat Datang di Kominfo Kabupaten Mojokerto</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="Vrinda Maru Kansal">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <!-- Favicone Icon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/img/favicon.ico') }}">
    <link rel="icon" type="image/png" href="{{ asset('/img/favicon.png') }}">
    <link rel="apple-touch-icon" href="{{ asset('/img/favicon.png') }}">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/ionicons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/plugin/sidebar-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/plugin/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/jquery-ui.css') }}">
    <!-- SLIDER REVOLUTION CSS SETTINGS -->
    <link href="{{ asset('/plugin/rs-plugin/css/settings.css') }}" rel="stylesheet" type="text/css" media="screen" />

</head>

<body style="overflow:hidden;">

    <!-- Preloader -->
    <section id="preloader">
        <div class="loader" id="loader">
            <div class="loader-img"></div>
        </div>
    </section>
    <!-- End Preloader -->

    <!-- Site Wraper -->
    <div class="wrapper">

        <!-- Header -->
        <header id="header" class="header shadow">
            <div class="header-inner">

                <!-- Logo -->
                <div class="logo">
                    <a href="/">
                        <img class="logo-light" src="img/kominfo.png" alt="Global Talent House" />
                    </a>
                </div>
                <!-- End Logo -->

                <!-- Mobile Navbar Icon -->
                <div class="nav-mobile nav-bar-icon">
                    <span></span>
                </div>
                <!-- End Mobile Navbar Icon -->

                <!-- Navbar Navigation -->
                <div class="nav-menu">
                    <ul class="nav-menu-inner">
                        <li>
                            <a class="" href="/">Home</a>
                        </li>
                        <li>
                            <a class="menu-has-sub">Category <i class="fa fa-angle-down"></i></a>
							<!-- Dropdown -->
                            <ul class="sub-dropdown dropdown">
								<li>
                                    <a href="/kategori">Hardware</a>
                                </li>
                                <li>
                                    <a href="/software">Software</a>
                                </li>
								<li>
                                    <a href="/prokum">Produk Hukum</a>
                                </li>
								<li>
                                    <a href="/ip">Ip Local</a>
                                </li>
                            </ul>
                            <!-- End Dropdown -->
                        </li>
                        <li>
                            <a class="" href="/inventori">Inventori</a>
                        </li>
                        <li>
                            <a class="" href="/contact">Contact Us</a>
                        </li>
                    </ul>
                </div>
                <!-- End Navbar Navigation -->

            </div>
        </header>
        <!-- End Header -->

        <!-- CONTENT --------------------------------------------------------------------------------->
        <!-- Intro -->
        <div id="intro" class="section">
			<div class="intro">
            <!-- Revolution Bg Video -->
            <div class="tp-banner-container">
                <div class="intro-Rev_Video text-center dark-bg black-bg">
                    <ul>
                        <li data-transition="fadein" data-slotamount="default" data-saveperformance="off" data-fsslotamount="7">

                            <!-- MAIN IMAGE -->
                            <img src="media/video1_poster.jpg" alt="video_forest" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->

                            <!-- Video Bg-->
                            <div class="tp-caption fullscreenvideo"
                                data-x="0"
                                data-y="0"
                                data-speed="0"
                                data-start="0"
                                data-easing="easeNone"
                                data-endspeed="1500"
                                data-endeasing="easeNone"
                                data-autoplay="true"
                                data-autoplayonlyfirsttime="false"
                                data-nextslideatend="true"
                                data-videowidth="100%"
                                data-videoheight="100%"
                                data-forcecover="1"
                                data-dottedoverlay="twoxtwo"
                                data-aspectratio="16:9"
                                data-forcerewind="on"
                                data-volume="mute"
                                style="z-index: 2">


                                <video class="video-js vjs-default-skin" preload="" loop="" width="100%" height="100%"
                                    poster='media/video1_poster.jpg'>
                                    <source src="media/intro.mp4" type='video/mp4' />
                                    <source src='media/intro.webm' type='video/webm' />
                                    <source src='media/intro.ogv' type='video/ogg' />
                                </video>


                                <!-- LAYER NR. 1 -->
                                <h5 class="tp-caption customin customout sfb"
                                    data-x="center"
                                    data-hoffset="0"
                                    data-y="center"
                                    data-voffset="-80"
                                    data-speed="800"
                                    data-start="800"
                                    data-endspeed="500"
                                    data-customin="x:-0;y:-100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                                    data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                    data-easing="Linear.easeNone"
                                    data-elementdelay="0.1">SELAMAT DATANG DI
                                </h5>

                                <!-- LAYER NR. 2 -->
                                <h1 class="tp-caption customin customout sfb h1"
                                    data-x="center"
                                    data-hoffset="0"
                                    data-y="center"
                                    data-voffset="0"
                                    data-speed="800"
                                    data-start="1400"
                                    data-endspeed="500"
                                    data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                                    data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                    data-easing="Linear.easeNone"
                                    data-elementdelay="0.1"
                                    data-endeasing="Linear.easeNone">KOMINFO KABUPATEN MOJOKERTO
                                </h1>


                            </div>

                        </li>
                    </ul>
                </div>
            </div>
            <!-- End Revolution Bg Video -->
			</div>
        </div>
        <!-- End Intro -->
    </div>
    <!-- Site Wraper End -->


    <!-- JS -->

    <script type="text/javascript" src="{{ asset('/js1/jquery-1.11.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.fitvids.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.viewportchecker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.stellar.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.colorbox-min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/plugin/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/plugin/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.fs.tipper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/mediaelement-and-player.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/jplugin/sidebar-menu.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/revolution-slider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/theme.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/navigation.js') }}"></script>

</body>
</html>
