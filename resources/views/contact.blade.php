<!DOCTYPE html>
<html>
<head>
	<title>Contact Us</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/form.css') }}">
	<script type="text/javascript" src="{{ asset('/js/app.js') }}"></script>
	
	<!-- Favicone Icon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/img/favicon.ico') }}">
    <link rel="icon" type="image/png" href="{{ asset('/img/favicon.png') }}">
    <link rel="apple-touch-icon" href="{{ asset('/img/favicon.png') }}">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/ionicons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/plugin/sidebar-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/plugin/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/jquery-ui.css') }}">
    <!-- SLIDER REVOLUTION CSS SETTINGS -->
    <link href="{{ asset('/plugin/rs-plugin/css/settings.css') }}" rel="stylesheet" type="text/css" media="screen" />

</head>

<body style="overflow:hidden;">

    <!-- Preloader -->
    <section id="preloader">
        <div class="loader" id="loader">
            <div class="loader-img"></div>
        </div>
    </section>
    <!-- End Preloader -->

    <!-- Site Wraper -->
    <div class="wrapper">

        <!-- Header -->
        <header id="header" class="header shadow">
            <div class="header-inner">

                <!-- Logo -->
                <div class="logo">
                    <a href="/">
                        <img class="logo-light" src="img/kominfo.png" alt="Global Talent House" />
                    </a>
                </div>
                <!-- End Logo -->

                <!-- Mobile Navbar Icon -->
                <div class="nav-mobile nav-bar-icon">
                    <span></span>
                </div>
                <!-- End Mobile Navbar Icon -->

                <!-- Navbar Navigation -->
                <div class="nav-menu">
                    <ul class="nav-menu-inner">
                        <li>
                            <a class="" href="/"> <font color="gold">Home</font></a>
                        </li>
                        <li>
                            <a class="menu-has-sub"> <font color="gold">Category <i class="fa fa-angle-down"> </font></i></a>
							<!-- Dropdown -->
                            <ul class="sub-dropdown dropdown">
								<li>
                                    <a href="/kategori"> Hardware</a>
                                </li>
                                <li>
                                    <a href="/software">Software</a>
                                </li>
								<li>
                                    <a href="/prokum">Produk Hukum</a>
                                </li>
								<li>
                                    <a href="/ip">Ip Local</a>
                                </li>
                            </ul>
                            <!-- End Dropdown -->
                        </li>
                        <li>
                            <a class="" href="/inventori"> <font color="gold">Inventori</font></a>
                        </li>
                        <li>
                            <a class="" href="/contact"> <font color="gold">Contact Us</font></a>
                        </li>
                    </ul>
                </div>
                <!-- End Navbar Navigation -->

            </div>
        </header>

</br></br></br></br></br></br>
        
<h2 align="center">Silakan isi form di bawah ini jika ada saran atau kritikan</h2>
 
</br>

 <form id="kontak-arlina" name="contact-form">
  
 <input id="ContactForm1_contact-form-name" name="name" placeholder="Name *" size="30" type="text" value="" />
  
 <input id="ContactForm1_contact-form-email" name="email" size="30" placeholder="Email *" type="text" value="" />
  
 <textarea cols="25" id="ContactForm1_contact-form-email-message" name="email-message" placeholder="Message *" rows="5"></textarea>
 <input id="ContactForm1_contact-form-submit" type="button" value="Send Message" />  
 <div style="max-width: 100%; text-align: center; width: 100%;">
 <div id="ContactForm1_contact-form-error-message">
 </div>
 <div id="ContactForm1_contact-form-success-message">
 </div>
 </div>
 </form>
 <style scoped="" type="text/css">
 #comments,.post_meta,#blog-pager{display:none}
 form{color:#666}
 form.payforpal{margin:auto;text-align:center}
 #kontak-arlina{margin:auto;max-width:640px}
 #ContactForm1_contact-form-name,#ContactForm1_contact-form-email{height:auto;margin:5px auto;padding:15px 12px;background:#fff;color:#444;border:1px solid rgba(0,0,0,.14);box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);border-radius:3px;width:100%;min-width:100%;transition:all 0.5s ease-out}
 #ContactForm1_contact-form-email-message{width:100%;height:175px;margin:5px 0;padding:15px 12px;background:#fff;color:#444;border:1px solid rgba(0,0,0,.14);box-shadow:inset 0 1px 1px rgba(0,0,0,0.075);border-radius:3px;resize:none;transition:all 0.5s ease-out}
 #ContactForm1_contact-form-name:focus,#ContactForm1_contact-form-email:focus,#ContactForm1_contact-form-email-message:focus{outline:none;background:#fff;color:#444;border-color:#66afe9;box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,0.6)}
 #ContactForm1_contact-form-submit{float:left;background:#1abc9c;margin:auto;vertical-align:middle;cursor:pointer;padding:16px 20px;font-size:14px;text-align:center;letter-spacing:.5px;border:0;width:100%;border-radius:4px;color:#fff;font-weight:500;transition:all .2s ease}
 #ContactForm1_contact-form-submit:hover{background:#16a085;color:#fff;}
 #ContactForm1_contact-form-error-message,#ContactForm1_contact-form-success-message{width:100%;margin-top:35px}
 .contact-form-error-message-with-border{background:#f47669;border:0;box-shadow:none;color:#fff;padding:5px 0;border-radius:3px}
 .contact-form-success-message{background:#4fc3f7;border:0;box-shadow:none;color:#fff;border-radius:3px}
 img.contact-form-cross{line-height:40px;margin-left:5px}
 .post-body input{width:initial}
 @media only screen and (max-width:640px){
 #ContactForm1_contact-form-name, #ContactForm1_contact-form-email,#ContactForm1_contact-form-submit{width:100%;}}
 </style>
 <script type="text/javascript">
 //<![CDATA[
 if (window.jstiming) window.jstiming.load.tick('widgetJsBefore');
 //]]>
 </script>
 <script src="https://www.blogger.com/static/v1/widgets/2271878333-widgets.js" type="text/javascript"></script>
 <script type="text/javascript">
 //<![CDATA[
 if (typeof(BLOG_attachCsiOnload) != 'undefined' && BLOG_attachCsiOnload != null) { window['blogger_templates_experiment_id'] = "templatesV1";window['blogger_blog_id'] = '8258252795368451266';BLOG_attachCsiOnload(''); }_WidgetManager._Init('//www.blogger.com/rearrange?blogID\x3d8258252795368451266','//agung19254-umsida.blogspot.com/','8258252795368451266');
 _WidgetManager._RegisterWidget('_ContactFormView', new _WidgetInfo('ContactForm1', 'footer1', null, document.getElementById('ContactForm1'), {'contactFormMessageSendingMsg': 'Sending...', 'contactFormMessageSentMsg': 'Your message has been sent.', 'contactFormMessageNotSentMsg': 'Message could not be sent. Please try again later.', 'contactFormInvalidEmailMsg': 'A valid email address is required.', 'contactFormEmptyMessageMsg': 'Message field cannot be empty.', 'title': 'Contact Form', 'blogId': '8258252795368451266', 'contactFormNameMsg': 'Name', 'contactFormEmailMsg': 'Email', 'contactFormMessageMsg': 'Message', 'contactFormSendMsg': 'Send', 'submitUrl': 'https://www.blogger.com/contact-form.do'}, 'displayModeFull'));
 //]]>
 </script></br>

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31647.605380061323!2d112.41983295126641!3d-7.470699653573348!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e780d8e8df8a9ad%3A0xe370efa72c0901bc!2sKota%20Mojokerto%2C%20Jawa%20Timur!5e0!3m2!1sid!2sid!4v1633919771488!5m2!1sid!2sid" width="600" height="560" frameborder="0" style="border:0; width: 100%;" allowfullscreen></iframe>

<!-- JS -->

    <script type="text/javascript" src="{{ asset('/js1/jquery-1.11.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.fitvids.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.viewportchecker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.stellar.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.colorbox-min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/plugin/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/plugin/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.fs.tipper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/mediaelement-and-player.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/jplugin/sidebar-menu.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/revolution-slider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/theme.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/navigation.js') }}"></script>

    <p class="copyright_text" align="center">Copyright 2021 All Right Reserved By PKL DISKOMINFO Kabupaten Mojokerto.</p>
</body>
</html>