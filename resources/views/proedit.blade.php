<!DOCTYPE html>
<html>
<head>
	<title>Edit Daftar Hukum</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/form.css') }}">
	<script type="text/javascript" src="{{ asset('/js/app.js') }}"></script>
	
	<!-- Favicone Icon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/img/favicon.ico') }}">
    <link rel="icon" type="image/png" href="{{ asset('/img/favicon.png') }}">
    <link rel="apple-touch-icon" href="{{ asset('/img/favicon.png') }}">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/ionicons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/plugin/sidebar-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/plugin/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/jquery-ui.css') }}">
    <!-- SLIDER REVOLUTION CSS SETTINGS -->
    <link href="{{ asset('/plugin/rs-plugin/css/settings.css') }}" rel="stylesheet" type="text/css" media="screen" />

</head>
<body>

	<!-- Site Wraper -->
    <div class="wrapper">

        <!-- Header -->
        <header id="header" class="header shadow">
            <div class="header-inner">

                <!-- Logo -->
                <div class="logo">
                    <a href="/">KOMINFO</a>
                </div>
                <!-- End Logo -->

                <!-- Mobile Navbar Icon -->
                <div class="nav-mobile nav-bar-icon">
                    <span></span>
                </div>
                <!-- End Mobile Navbar Icon -->

                <!-- Navbar Navigation -->
                <div class="nav-menu">
                    <ul class="nav-menu-inner">
                        <li>
                            <a class="" href="/"> <font color="gold">Home</font></a>
                        </li>
                        <li>
                            <a class="menu-has-sub"> <font color="gold">Category <i class="fa fa-angle-down"> </font></i></a>
							<!-- Dropdown -->
                            <ul class="sub-dropdown dropdown">
								<li>
                                    <a href="/kategori"> Hardware</a>
                                </li>
                                <li>
                                    <a href="/software">Software</a>
                                </li>
								<li>
                                    <a href="/prokum">Produk Hukum</a>
                                </li>
								<li>
                                    <a href="/ip">Ip Local</a>
                                </li>
                            </ul>
                            <!-- End Dropdown -->
                        </li>
                        <li>
                            <a class="" href="/inventori"> <font color="gold">Inventori</font></a>
                        </li>
                        <li>
                            <a class="" href="/contact"> <font color="gold">Contact Us</font></a>
                        </li>
                    </ul>
                </div>
                <!-- End Navbar Navigation -->

            </div>
        </header>
        <!-- End Header -->
	<br/><br/><br/><br/><br/>
	<h2 align="center">KOMINFO KABUPATEN MOJOKERTO</h2>
	<h3 align="center">Edit Daftar Hukum</h3>

	@foreach($produk_hukum as $p)
	<form action="/prokum/update" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $p->hukum_id }}"> <br/>
		<label for="nama">Nama</label>
		<input type="text" required="required" name="nama" value="{{ $p->hukum_nama }}"> <br/>
		<button  type="submit" value="Simpan Data">Simpan Data </button>
		<a href="/prokum" class="btn fourth"> Kembali</a>
	</form>
	@endforeach
		
<!-- JS -->

	<script type="text/javascript" src="{{ asset('/js1/jquery-1.11.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.fitvids.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.viewportchecker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.stellar.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.colorbox-min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/plugin/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/plugin/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.fs.tipper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/mediaelement-and-player.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/jplugin/sidebar-menu.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/revolution-slider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/theme.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/navigation.js') }}"></script>


</body>
</html>