<!DOCTYPE html>
<html>
<head>
	<title>inventori</title>
	<script type="text/javascript" src="{{ asset('/js/app.js') }}"></script>
	
	<!-- Favicone Icon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/img/favicon.ico') }}">
    <link rel="icon" type="image/png" href="{{ asset('/img/favicon.png') }}">
    <link rel="apple-touch-icon" href="{{ asset('/img/favicon.png') }}">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/ionicons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/plugin/sidebar-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/plugin/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css1/jquery-ui.css') }}">
    <!-- SLIDER REVOLUTION CSS SETTINGS -->
    <link href="{{ asset('/plugin/rs-plugin/css/settings.css') }}" rel="stylesheet" type="text/css" media="screen" />

    <style type="text/css">
		
        table th {
        background:gold;
        color:#DCDCDC;
        font-weight:bold;
        font-size:14px;
        text-align: center;
        }

        /* Mengatur warna baris */
        table tr {
        background:#ADE8E6;
        font-size: 15px;
        }
        /* Mengatur warna baris genap (akan menghasilkan warna selang seling pada baris tabel) */
        table tr:nth-child(even) {
        background:#E0FFFF;
        }
        
        .table {
        width: 80%;
        background: #fff;
        text-align: center; 
        position : Relative;
        Border : Collapse;
        Margin : auto
        }
        .table thead.thead-primary {
            background: red; }
        .table thead th {
            border: none;
            font-size: 16px;
            color: #fff; }

	</style>

</head>
<body>

	<!-- Site Wraper -->
    <div class="wrapper">

        <!-- Header -->
        <header id="header" class="header shadow">
            <div class="header-inner">

                <!-- Logo -->
                <div class="logo">
                    <a href="/">
                        <img class="logo-light" src="img/kominfo.png" alt="KOMINFO"/>
                    </a>
                </div>
                <!-- End Logo -->

                <!-- Mobile Navbar Icon -->
                <div class="nav-mobile nav-bar-icon">
                    <span></span>
                </div>
                <!-- End Mobile Navbar Icon -->

                <!-- Navbar Navigation -->
                <div class="nav-menu">
                    <ul class="nav-menu-inner">
                        <li>
                            <a class="" href="/"> <font color="gold">Home</font></a>
                        </li>
                        <li>
                            <a class="menu-has-sub"> <font color="gold">Category <i class="fa fa-angle-down"> </font></i></a>
							<!-- Dropdown -->
                            <ul class="sub-dropdown dropdown">
								<li>
                                    <a href="/kategori"> Hardware</a>
                                </li>
                                <li>
                                    <a href="/software">Software</a>
                                </li>
								<li>
                                    <a href="/prokum">Produk Hukum</a>
                                </li>
								<li>
                                    <a href="/ip">Ip Local</a>
                                </li>
                            </ul>
                            <!-- End Dropdown -->
                        </li>
                        <li>
                            <a class="" href="/inventori"> <font color="gold">Inventori</font></a>
                        </li>
                        <li>
                            <a class="" href="/contact"> <font color="gold">Contact Us</font></a>
                        </li>
                    </ul>
                </div>
                <!-- End Navbar Navigation -->

            </div>
        </header>
        <!-- End Header -->

	<br/><br/><br/><br/><br/>
	<h2 align="center">INVENTORI DISKOMINFO KABUPATEN MOJOKERTO</h2>

    </h6>
    
        <div class="table-wrap">
				<table class="table">
			    <thead class="thead-primary">
            <tr>
                <th>ID</th>
                <th>NAMA BARANG</th>
                <th>STOK</th>
                <th>KETERANGAN</th>
                
            </tr>
                @foreach($Hardware as $p)
            <tr>
                <td>AH0{{ $p->hardware_id }}</td>
                <td>{{ $p->hardware_nama }}</td>
                <td>{{ $p->hardware_stok }}</td>
                <td>{{ $p->hardware_keterangan }}</td>
                @endforeach</tr><tr>
                @foreach($software as $s)
                <td>AS0{{ $s->software_id }}</td>
                <td>{{ $s->software_nama }}</td>
                <td>{{ $s->software_stok }}</td>
                <td>{{ $s->software_keterangan }}</td>
                </tr>
                @endforeach
                @foreach($produk_hukum as $p)
                <tr>
                    <td>PH0{{ $p->hukum_id }}</td>
                    <td>{{ $p->hukum_nama }}</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
                @endforeach
                @foreach($ip_local as $p)
                <tr>
                    <td>IP0{{ $p->ip_id }}</td>
                    <td>{{ $p->ip_nama }}</td>
                    <td>-</td>
                    <td>{{ $p->ip_nomor }}</td>
                </tr>
		@endforeach
        </tbody>
	    </table>
		</div>


<!-- JS -->

    <script type="text/javascript" src="{{ asset('/js1/jquery-1.11.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.easing.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.fitvids.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.viewportchecker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.stellar.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.colorbox-min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/plugin/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/plugin/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('/js1/plugin/jquery.fs.tipper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/plugin/mediaelement-and-player.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/jplugin/sidebar-menu.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/revolution-slider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/theme.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js1/navigation.js') }}"></script>
    </br>

    <p class="copyright_text" align="center">Copyright 2021 All Right Reserved By PKL DISKOMINFO Kabupaten Mojokerto.</p>
</body>
</html>